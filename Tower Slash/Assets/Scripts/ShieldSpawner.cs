using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldSpawner : MonoBehaviour
{
    [SerializeField] GameObject shieldPrefab;
    int multiplier = 1;
    public GameManager game;

    // Start is called before the first frame update
    void Start()
    {
        game = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //spawn shield every 25 points
        if (game.points > 5*multiplier)
        {
            GameObject newShield = (GameObject)Instantiate(shieldPrefab, new Vector3(1.24f, 5.5f, 0), Quaternion.identity);
            multiplier += 5;
        }
    }


}
