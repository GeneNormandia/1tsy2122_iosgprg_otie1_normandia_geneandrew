using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DkSpawner : MonoBehaviour
{
    [SerializeField] GameObject dkPrefab;
    int multiplier = 1;
    public GameManager game;

    // Start is called before the first frame update
    void Start()
    {
        game = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (game.points > 10 * multiplier)
        {
            GameObject newDk = (GameObject)Instantiate(dkPrefab, new Vector3(1.24f, 5.5f, 0), Quaternion.identity);
            multiplier += 1;
        }
    }
}
