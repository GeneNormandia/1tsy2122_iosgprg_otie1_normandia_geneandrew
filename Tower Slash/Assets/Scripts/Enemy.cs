using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health;
    public float attack;
    public float def;
   
    float speedM = .03f;

    public GameManager game;
    // Start is called before the first frame update
    void Start()
    {
        game = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= new Vector3(0, speedM, 0);
        if (game.points >= 34 && game.points <= 44) //perform dash
        {
            speedM = .1f;
        }
        else 
        {
            speedM = .03f;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "collider")
        {
            game.health -= 1;
        }
    }
}
