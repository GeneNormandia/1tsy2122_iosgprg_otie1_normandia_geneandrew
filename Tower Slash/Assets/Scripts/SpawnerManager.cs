using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    [SerializeField] List<Enemy> enemyList = new List<Enemy>();

    [SerializeField] GameObject enemyPrefab;

    float randX = 50;
    float randY = 50;

    float timer;

    public GameManager game;

    void SpawnEnemy()
    {
        float posX = 1.24f;
        float posY = 5.5f;

        GameObject newEnemy = (GameObject)Instantiate(enemyPrefab, new Vector3(posX, posY, 0), Quaternion.identity);
        AddEnemyToList(newEnemy.GetComponent<Enemy>());
    }

    void AddEnemyToList(Enemy enemy)
    {
        enemyList.Add(enemy);
    }

    // Start is called before the first frame update
    void Start()
    {
        game = FindObjectOfType<GameManager>();
        timer = 2f;
        //StartCoroutine(Timer());
        SpawnEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        

        /*if (game.points >= 10) //perform combo
        {
            timer = .4f;
        }*/

        if (timer < 0)
        {
            SpawnEnemy();
            
            if (game.points >= 20 && game.points <= 30)//spawn combo
            {
                timer = .5f;
            }
            else
            {
                timer = Random.Range(.8f, 1.3f); //random time enemy spawns
            }
        }
    }

    IEnumerator Timer()
    {
            SpawnEnemy();
            yield return new WaitForSeconds(1f);
    }
}