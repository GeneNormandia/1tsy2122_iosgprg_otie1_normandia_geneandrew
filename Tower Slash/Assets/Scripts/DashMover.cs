using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashMover : MonoBehaviour
{
    public Text textDash;
    public GameManager game;
    // Start is called before the first frame update
    void Start()
    {
        game = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(game.points >= 30 && game.points <= 34) //perform dash
        {
            transform.position = new Vector3(.8f, -2f, 0);
            textDash.text = "Dashing through enemies...";
        }
        else if (game.points >= 45) //end dash
        {
            transform.position = new Vector3(0, 0, 0);
            textDash.text = "Dash ended";
        }
    }
}
