﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateJoystick : MonoBehaviour
{
	float horizontalMove = 0f, verticalMove = 0f;
	public Joystick  lookJoystick;

	void Update()
	{
		Vector3 moveVector = (Vector3.up * lookJoystick.Horizontal + Vector3.left * lookJoystick.Vertical);
		if (lookJoystick.Horizontal != 0 || lookJoystick.Vertical != 0)
		{
			transform.rotation = Quaternion.LookRotation(Vector3.forward, moveVector);
		}
	}

}

