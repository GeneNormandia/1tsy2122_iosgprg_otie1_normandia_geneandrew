﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolAmmo : MonoBehaviour
{
    public Fire gun;
    // Start is called before the first frame update
    void Start()
    {
        gun = FindObjectOfType<Fire>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (gun.currentWeapon == 1)
            {
                gun.currentAmmo += 10;
            }
        }
    }
}
