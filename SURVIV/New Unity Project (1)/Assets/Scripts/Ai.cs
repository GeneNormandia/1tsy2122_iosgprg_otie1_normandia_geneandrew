﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ai : MonoBehaviour
{
    public float speed;
    private float waitTime;
    public float startWaitTime;

    public Transform[] moveSpots;
    public int randomSpot;

    public Player player;

    int state;
    


    // Start is called before the first frame update
    void Start()
    {
        state = Random.Range(1, 3);
        waitTime = startWaitTime;
        randomSpot = Random.Range(0, moveSpots.Length);
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        //Idle
        if (state == 1)
        {
            //Debug.Log("Idle " + state);
        }

        //Patrol
        if (state > 1)
        {
            Debug.Log("Patrol " + state);
            transform.position = Vector2.MoveTowards(transform.position, moveSpots[randomSpot].position, speed * Time.deltaTime);

            if (Vector2.Distance(transform.position, moveSpots[randomSpot].position) < 0.2f)
            {
                if (waitTime <= 0)
                {
                    randomSpot = Random.Range(0, moveSpots.Length);
                    waitTime = startWaitTime;
                }
                else
                {
                    waitTime -= Time.deltaTime;
                }
            }
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Check if another AI is sighted
        if (other.CompareTag("Enemy"))
        {
            //Debug.Log("Detected Enemy");
            this.GetComponent<EnemyAtkEnemy>().enabled = true;
        }
        //Check if the player is sighted
        if (other.CompareTag("Player"))
        {
            //Debug.Log("Detected player");
            this.GetComponent<EnemyFire>().enabled = true;
        }
    }
}
