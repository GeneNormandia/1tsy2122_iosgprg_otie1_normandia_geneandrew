﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootSpawner : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] lootPrefabs;
    public int lootsToSpawn;
    // Start is called before the first frame update
    void Start()
    {
        int spawn = 0;
        for (int i = 0; i < lootsToSpawn; i++)
        {
            int randomLoot = Random.Range(0, lootPrefabs.Length);
            int randomSpawnPoint = Random.Range(0, spawnPoints.Length);

            Instantiate(lootPrefabs[randomLoot], spawnPoints[spawn].position, transform.rotation);
            spawn +=1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
