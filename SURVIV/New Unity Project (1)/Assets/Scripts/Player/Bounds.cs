﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounds : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(transform. position.y >= 16)
        {
            transform.position = new Vector3(transform.position.x, 16, 0);
        }
        if (transform.position.y <= -17)
        {
            transform.position = new Vector3(transform.position.x, -17, 0);
        }
        if (transform.position.x >= 32)
        {
            transform.position = new Vector3(32, transform.position.y, 0);
        }
        if (transform.position.x <= -34)
        {
            transform.position = new Vector3(-34, transform.position.y, 0);
        }
    }
}
