﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private HealthBar healthBar;
    int health = 10;
    public int points;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        healthBar.SetSize(health * .1f);
        Debug.Log("Points " + points);

        if(points >= 10)
        {
            SceneManager.LoadScene("WinScreen");
        }
        if (health <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EnemyBullet"))
        {
            health -= 1;
        }
    }

}
