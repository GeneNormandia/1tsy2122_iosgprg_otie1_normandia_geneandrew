﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fire : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D bullet;

    [SerializeField]
    private Transform nozzle;

    [SerializeField] private Transform nozzle2;

    [SerializeField] private Transform nozzle3;

    //UI
    public Text bulletDisplay;
    public Text clipDisplay;


    //Stats
    private float bulletSpeed = 500f;

    public int currentWeapon;
    public int currentWeaponTracker;

    public string weaponName = "test";
    float fireRate = 1.1f;

    public int currentAmmo;
    public int currentClips;

    int ammoTracker;
    int clipsTracker;

    int secondaryAmmo;
    int secondaryClips;

    bool isFiring;
    bool stopFiring;

    //Secondary weapon
    int secondaryWeapon = 2;



    // Start is called before the first frame update
    void Start()
    {
        currentWeapon = 3;
        updateStats();

        currentAmmo = 10;
        currentClips = 1;

        secondaryAmmo = 10;
        secondaryClips = 2;


        ammoTracker = currentAmmo;
        clipsTracker = currentClips;
    }

    // Update is called once per frame
    void Update()
    {
        //Update Ui
        Text primaryWeapon = GameObject.Find("Canvas/PrimaryWeapon").GetComponent<Text>();
        primaryWeapon.text = weaponName;

        bulletDisplay.text = currentAmmo.ToString();
        clipDisplay.text = currentClips.ToString();

        currentWeaponTracker = currentWeapon;
        ammoTracker = currentAmmo;
        clipsTracker = currentClips;

        //Update weapon stats
        //updateStats();

        if (isFiring)
        {
            makeFireVariableFalse();
            
            Shoot();
        }

        //Reload gun if no ammo left and there are clips
        if (currentAmmo <= 0)
        {
            if (currentClips != 0)
            {
                StartCoroutine(Timedelay());
            }
        }
    }

    public void pointerDown()
    {
        //isFiring = true;
        stopFiring = false;
        Invoke("makeFireVariableTrue", 0.3f);
    }

    public void pointerUp()
    {
        isFiring = false;
        stopFiring = true;
    }

    void makeFireVariableTrue()
    {
        isFiring = true;
    }

    void makeFireVariableFalse()
    {
        isFiring = false;
        if (stopFiring == false)
        {
            Invoke("makeFireVariableTrue", fireRate);
        }
    }

    public void updateStats()
    {
        if(currentWeapon == 1)
        {
            weaponName = "Pistol";
            fireRate = 1.0f;
            //currentAmmo = 20;
            //currentClips = 3;
            currentWeaponTracker = 1;
            
        }
        if (currentWeapon == 2)
        {
            weaponName = "Shotgun";
            fireRate = 0.5f;
            //currentAmmo = 10;
            //currentClips = 3;
            currentWeaponTracker = 2;
        }
        if (currentWeapon == 3)
        {
            weaponName = "Rifle";
            fireRate = 0.1f;
            //currentAmmo = 50;
            //currentClips = 2;
            currentWeaponTracker = 3;
        }
    }
  

    public void Shoot()
    {
         WeaponFire();
    }


    void WeaponFire()
    {
        if (currentWeapon == 1)
        {
            if(currentAmmo != 0)
            {
                var spawnedBullet = Instantiate(bullet, nozzle.position, nozzle.rotation);
                spawnedBullet.AddForce(nozzle.up * bulletSpeed);
                currentAmmo -= 1;
            }
            
        }
        else if(currentWeapon == 2)
        {
            if(currentAmmo != 0)
            {
                var spawnedBullet = Instantiate(bullet, nozzle.position, nozzle.rotation);
                spawnedBullet.AddForce(nozzle.up * bulletSpeed);
                var spawnedBullet2 = Instantiate(bullet, nozzle2.position, nozzle2.rotation);
                spawnedBullet2.AddForce(nozzle2.up * bulletSpeed);
                var spawnedBullet3 = Instantiate(bullet, nozzle3.position, nozzle3.rotation);
                spawnedBullet3.AddForce(nozzle3.up * bulletSpeed);
                currentAmmo -= 1;
            }  
        }
        else if (currentWeapon == 3)
        {
            
            if (currentAmmo != 0)
            { 
                var spawnedBullet = Instantiate(bullet, nozzle.position, nozzle.rotation);
                spawnedBullet.AddForce(nozzle.up * bulletSpeed);
                currentAmmo -= 1;
            }
        }
    }

    void Reload()
    {
        if(currentAmmo <= 0)
        {
            if (currentClips != 0)
            {
                StartCoroutine(Timedelay());
                currentClips -= 1;
                currentAmmo += 30;
            }
        }
    }
    IEnumerator Timedelay()
    {
        yield return new WaitForSeconds(3);
        if (currentClips != 0)
        {
            StartCoroutine(Timedelay());
            currentClips -= 1;
            currentAmmo += 30;
        }

    }
    public void SwitchWeapon()
    {
        //Switch the primary weapon with the secondary weapon
        currentWeapon = secondaryWeapon;
        secondaryWeapon = currentWeaponTracker;



        //Switch primary ammo with secondary ammo
        currentAmmo = secondaryAmmo;
        secondaryAmmo = ammoTracker;
        


        updateStats();
    }

    public void AddAmmo()
    {
        currentAmmo += 10;
        currentClips += 1;
    }
}