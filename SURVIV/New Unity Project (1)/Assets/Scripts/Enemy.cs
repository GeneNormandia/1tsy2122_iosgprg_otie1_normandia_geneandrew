﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Player player;
    int health = 10;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            player.points += 1;
            Destroy(transform.parent.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Check if another AI is sighted
        if (other.CompareTag("Bullet"))
        {
            health -= 1;
        }
    }

}
