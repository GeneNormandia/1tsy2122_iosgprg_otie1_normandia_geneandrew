﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Automatic : MonoBehaviour
{
    public Fire gun;
    // Start is called before the first frame update
    void Start()
    {
        gun = FindObjectOfType<Fire>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Collided");
            gun.currentWeapon = 3;
            gun.currentAmmo = 30;
            gun.currentClips = 2;
            Destroy(gameObject);
            gun.updateStats();
        }
    }
}
