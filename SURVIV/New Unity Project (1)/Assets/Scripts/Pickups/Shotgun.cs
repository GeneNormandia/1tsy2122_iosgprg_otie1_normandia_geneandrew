﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : MonoBehaviour
{
    public Fire gun;
    // Start is called before the first frame update
    void Start()
    {
        gun = FindObjectOfType<Fire>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Collided");
            gun.currentWeapon = 2;
            gun.currentAmmo = 15;
            gun.currentClips = 1;
            Destroy(gameObject);
            gun.updateStats();
        }
    }
}
