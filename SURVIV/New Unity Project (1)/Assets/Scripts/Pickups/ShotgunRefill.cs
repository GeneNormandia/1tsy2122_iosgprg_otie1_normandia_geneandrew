﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunRefill : MonoBehaviour
{
    public Fire gun;

    // Start is called before the first frame update
    void Start()
    {
        gun = FindObjectOfType<Fire>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //Check if the current weapon is applicable for the bullet
            if (gun.currentWeapon == 2)
            {
                gun.AddAmmo();
                Destroy(gameObject);
            }
        }
    }
}
